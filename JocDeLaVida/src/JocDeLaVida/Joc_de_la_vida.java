package JocDeLaVida;

/*REGLAS:
 * - Una celula viva amb menys de dues veïnes vives, mor per soletat en la següent generació.
 * - Una celula viva amb dos o tres veïnes vives viu en la següent generació.
 * - Una celula viva amb més de tres veïnes vives, mor per superpoblació.
 * - Una celula morta amb exactament tres veïnes vives resucita en la següent generació.
 */

import java.util.Scanner;

public class Joc_de_la_vida {
	static Scanner reader = new Scanner(System.in);

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int ncasos = 0, midatauler = 15, fila = 1, col = 1, generacions, numerocelules = 0;
		char[][] tauler1 = new char[midatauler][midatauler];
		char[][] tauler2 = new char[midatauler][midatauler];
		boolean stop = false;
		
		ncasos = reader.nextInt();
		
		while(ncasos > 0) {
			for(int i = 0; i < midatauler; i++) {
				for(int j = 0; j < midatauler; j++) {
					tauler1[i][j] = ' ';
					tauler2[i][j] = ' ';
				}
			}
			generacions = reader.nextInt();
			while(!stop) {
				fila = reader.nextInt();
				col = reader.nextInt();
				if (fila == 0 && col == 0) {
					stop = true;
				}else {
					if (tauler1[fila][col] == '■') {
						tauler1[fila][col] = ' ';
					}else {
						tauler1[fila][col] = '■';
					}
				}
			}
			
			for(int i = 0; i < midatauler; i++) {
				System.out.println("-------------------------------------------------------------");
				System.out.print("| ");
				for (int j = 0; j < midatauler; j++) {
					System.out.print(tauler1[i][j] + " | ");
				}
				System.out.println(" ");
				if(i == 14) {
					System.out.println("-------------------------------------------------------------");
				}
			}
			
			System.out.print("\n\nTauler passades " + generacions + " generacions:\n");
			
			while(generacions > 0) {
				for(int i = 0;i < midatauler; i++) {
					for(int j = 0; j < midatauler; j++) {
						numerocelules = 0;
						fila = i;
						col = j;
							if(fila == 0 && col == 0) {
								for (fila = 0;fila<2; fila++) {
									for (col = 0;col<2;col++) {
										if (tauler1[fila][col] == '■') {
											numerocelules++;
										}
									}
								}
							}else {
							
								if(fila == midatauler-1 && col == midatauler-1) {
									for (fila = midatauler-1;fila>midatauler-3; fila--) {
										for (col = midatauler-1;col>midatauler-3;col--) {
											if (tauler1[fila][col] == '■') {
												numerocelules++;
											}
										}
									}
								}else {
								
									if(fila == midatauler-1 && col == 0) {
										for (fila = midatauler-1;fila>midatauler-3; fila--) {
											for (col = 0;col< 2;col++) {
												if (tauler1[fila][col] == '■') {
													numerocelules++;
												}
											}
										}
									}else {
									
										if(fila == 0 && col == midatauler-1) {
											for (fila = 0;fila<2; fila++) {
												for (col = midatauler-1;col>midatauler-3;col--) {
													if (tauler1[fila][col] == '■') {
														numerocelules++;
													}
												}
											}
										}else {
										
											if(fila == 0 && col>0 && col<midatauler-1) {
												if (tauler1[fila][col+1]=='■') {
													numerocelules++;
												}
												if (tauler1[fila][col-1] == '■') {
													numerocelules++;
												}
												if (tauler1[fila+1][col] == '■') {
													numerocelules++;
												}
												if (tauler1[fila+1][col+1] == '■') {
													numerocelules++;
												}
												if (tauler1[fila+1][col-1] == '■') {
													numerocelules++;
												}
											}else {
											
												if(fila == midatauler-1 && col>0 && col<midatauler-1) {
													if (tauler1[fila][col+1] == '■') {
														numerocelules++;
													}
													if (tauler1[fila][col-1] == '■') {
														numerocelules++;
													}
													if (tauler1[fila-1][col] == '■') {
														numerocelules++;
													}
													if (tauler1[fila-1][col+1] == '■') {
														numerocelules++;
													}
													if (tauler1[fila-1][col-1] == '■') {
														numerocelules++;
													}
												}else {
												
													if(fila >0 && fila<midatauler-1 && col == 0) {
														if (tauler1[fila+1][col] == '■') {
															numerocelules++;
														}
														if (tauler1[fila-1][col] == '■') {
															numerocelules++;
														}
														if (tauler1[fila][col+1] == '■') {
															numerocelules++;
														}
														if (tauler1[fila+1][col+1] == '■') {
															numerocelules++;
														}
														if (tauler1[fila-1][col+1] == '■') {
															numerocelules++;
														}
													}else {
													
														if(fila >0 && fila<midatauler-1 && col == midatauler-1) {
															if (tauler1[fila+1][col] == '■') {
																numerocelules++;
															}
															if (tauler1[fila-1][col] == '■') {
																numerocelules++;
															}
															if (tauler1[fila][col-1] == '■') {
																numerocelules++;
															}
															if (tauler1[fila+1][col-1] == '■') {
																numerocelules++;
															}
															if (tauler1[fila-1][col-1] == '■') {
																numerocelules++;
															}
														}else {
														
															if(fila > 0 && fila < midatauler-1 && col > 0 && col < midatauler-1) {
																if(tauler1[fila][col+1] == '■') {
																	numerocelules++;
																}
																if(tauler1[fila][col-1] == '■') {
																	numerocelules++;
																}
																if(tauler1[fila+1][col] == '■') {
																	numerocelules++;
																}
																if(tauler1[fila+1][col+1] == '■') {
																	numerocelules++;
																}
																if(tauler1[fila+1][col-1] == '■') {
																	numerocelules++;
																}
																if(tauler1[fila-1][col] == '■') {
																	numerocelules++;
																}
																if(tauler1[fila-1][col+1] == '■') {
																	numerocelules++;
																}
																if(tauler1[fila-1][col-1] == '■') {
																	numerocelules++;
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
							if(tauler1[i][j] == '■') {
								if(numerocelules < 2) {
									tauler2[i][j] = ' ';
								}else {
									if(numerocelules == 2 || numerocelules == 3) {
										tauler2[i][j] = '■';
									}else {
										if(numerocelules > 3) {
											tauler2[i][j] = ' ';
										}
									}
								}
							}else {
								if(tauler1[i][j] == ' ') {
									if(numerocelules == 3) {
										tauler2[i][j] = '■';
									}else tauler2[i][j] = ' ';
								}
							}
					}
				}
				
				for (int i = 0; i<midatauler; i++) {
					for(int j = 0; j < midatauler; j++) {
						tauler1[i][j] = tauler2[i][j];
						tauler2[i][j] = ' ';
					}
				}
				generacions--;
				
			}
			
			
			ncasos--;
		}
		
		for(int i = 0; i < midatauler; i++) {
			System.out.println("-------------------------------------------------------------");
			System.out.print("| ");
			for (int j = 0; j < midatauler; j++) {
				System.out.print(tauler1[i][j] + " | ");
			}
			System.out.println(" ");
			if(i == 14) {
				System.out.println("-------------------------------------------------------------");
			}
		}

	}

	public static int obtenirnumero() {
		int numero = 0;
		numero = reader.nextInt();
		return numero;
	}
}
